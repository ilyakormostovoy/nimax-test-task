function Table(id) {
    this.$el = $("#" + id);
    this.$clear = this.$el.find(".js-table-clear");
    this.$list = this.$el.find(".table__list");
    this.rows = [];
    this.bind();
}

Table.prototype.bind = function() {
    let self = this;

    this.$el.on("ROW:DELETE", function () {
        self.deleteRow.apply(self, arguments);
        self.changed();
    });

    this.$el.on("ROW:CHANGED", function() {
        self.changed();
    });

    this.$clear.on("click", function () {
        self.clear();
    });
};

Table.prototype.changed = function() {
    let sum = this.getAllSummary();
    $(window).trigger("TABLE:CHANGED", sum);
};

Table.prototype.clear = function() {
    let self = this;

    $.each(this.rows, function(index, row) {
        if (row !== undefined) self.deleteRow(null, row);
    });

    this.changed();
};

Table.prototype.deleteRow = function(e, row) {
    let rowId = row.data.id;
    let offset = this.getProductById(rowId).index;

    row.delete();

    delete this.rows[offset];
};

Table.prototype.makeRow = function(data) {
    let offset = this.rows.push(new Row(data));
    this.$list.append(this.rows[offset - 1].$el);
};

Table.prototype.getProductById = function(id) {
    let index = 0;

    this.rows.forEach(function(item, idx) {
        if (item.data.id === id) {
            index = idx;

            return false;
        }
    });

    return { row: this.rows[index], index: index };
};

Table.prototype.fill = function(data) {
    let self = this;

    data.forEach(function (item) {
        self.makeRow(item);
    });

    this.changed();
};

Table.prototype.getAllSummary = function() {
    let sumAll = 0;
    this.rows.forEach(function(item) {
        sumAll += +item.data.summary;
    });

    return sumAll;
};

Table.prototype.formatPrice = function(num) {

    if (num < 10000) return num;

    let n = num.toString();
    let separator = " ";

    return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + separator);
};
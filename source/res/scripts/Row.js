let model = {
    id: "",
    name: "",
    dimensions: "",
    weight: "",
    price: "0",
    count: "1",
    summary: "0"
};

function Row(item) {
    this.$el = $(this.getTemplate());
    this.$delete = this.$el.find('.js-delete-row');
    this.$plus = this.$el.find('.js-plus');
    this.$minus = this.$el.find('.js-minus');
    this.$summary = this.$el.find('.js-field-summary');
    this.data = $.extend({}, model, item);
    this.bind();
    this.render();
    this.calcSum();
}

Row.prototype.bind = function() {
    let self = this;

    this.$delete.on("click", function() {
        self.$el.trigger('ROW:DELETE', self);
    });

    this.$plus.on("click", function() {
        self.inc();
    });

    this.$minus.on("click", function() {
        self.dec();
    });
};

Row.prototype.inc = function() {
    let count = this.data.count ? +this.data.count + 1 : 1;

    this.disableBtn(count);
    this.set("count", count);
    this.calcSum();
};

Row.prototype.dec = function() {
    let count = this.data.count ? +this.data.count - 1 : 0;

    this.disableBtn(count);
    this.set("count", count);
    this.calcSum();
};

Row.prototype.disableBtn = function(count) {
    if (count == 0) {
        this.$minus.addClass("counter__button_disabled");
    } else {
        this.$minus.removeClass("counter__button_disabled");
    }
};

Row.prototype.calcSum = function() {
    let sum = this.data.count * this.data.price;

    this.set("summary", sum);
    this.$el.trigger("ROW:CHANGED", this);
};

Row.prototype.delete = function() {
    this.$el.remove();
};

Row.prototype.getTemplate = function() {
    return document.getElementById('row-template').innerHTML;
};

Row.prototype.set = function(name, value) {
    this.data[name] = value;

    this.renderField(name);
};

Row.prototype.renderField = function (field) {

    let className = ".js-field-" + field;
    let $field = this.$el.find(className);
    let value = isPrice(this.data[field]);

    if (!$field.is("input")) {
        value = value === 0 ? "—" : value;
        $field.html(value);
    } else {
        $field.val(value);
    }

    function isPrice(value) {
        if (field !== "price" && field !== "summary") {
            return value;
        } else {
            return Table.prototype.formatPrice(value);
        }
    }
};

Row.prototype.render = function() {
    let self = this;

    $.each(this.data, function(fieldName) {
        self.renderField(fieldName);
    });
};
function Summary(id) {
    this.$el = $("#" + id);
    this.$discountRow = this.$el.find(".js-discount-row");
    this.$discountSize = this.$el.find(".js-discount-size");
    this.$discountPrice = this.$el.find(".js-discount-price");
    this.$summary = this.$el.find(".js-sum");
    this.bind();
}

Summary.prototype.data = {
    sum: 0,
    discountSize: 0,
    discountPrice: 0
};

Summary.prototype.bind = function() {
    let self = this;

    $(window).on("TABLE:CHANGED", function(e, sum) {
        self.calc(sum);
        self.render();
    });
};

Summary.prototype.discounts = { 10000: 5, 15000: 10, 20000: 15 };

Summary.prototype.calc = function(sum) {
    let discountSize = 0;
    let discountPrice;

    $.each(this.discounts, function(index, item) {
        if (sum > index) {
            discountSize = item;
        }
    });

    discountPrice = sum - (sum * discountSize) / 100;

    if (sum === discountPrice) {
        $(this.$summary).parent().addClass('price-block__number_summary');
        $(this.$summary).parent().prev().text("Цена");
    } else {
        $(this.$summary).parent().removeClass('price-block__number_summary');
        $(this.$summary).parent().prev().text("Без скидки");
    }

    $.extend(this.data, {sum: sum, discountSize: discountSize, discountPrice: discountPrice});
};

Summary.prototype.renderField = function(field) {
    let className = ".js-" + field;
    let $field = this.$el.find(className);
    let value = this.data[field];

    $field.html(Table.prototype.formatPrice(value));
};

Summary.prototype.render = function() {
    let self = this;

    if (this.data.discountSize > 0) {
        this.$discountRow.show();
    } else {
        this.$discountRow.hide();
    }

    $.each(this.data, function(fieldName) {
        self.renderField(fieldName);
    });
};
"use strict";

(function() {

    let ROOT = "";
    let RES = ROOT + "/json/product.json";
    let table = new Table("table");
    let summary = new Summary("summary");
    // summary.discounts = { 5000: 5, 15000: 10, 20000: 15 };

    function fetchData() {
        return fetch(RES).then(function(response) {
            return response.json();
        });
    }

    function main(data) {
        table.fill(data);
    }

    fetchData().then(main);

})();
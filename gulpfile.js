"use strict";

let gulp = require('gulp'),
    scss = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require("gulp-concat"),
    browserSync = require('browser-sync').create();

// Browser-sync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('source/*.html', ['html']);
    gulp.watch('source/res/styles/*.scss', ['scss']);
    gulp.watch('source/res/scripts/*.js', ['js']);
});

// HTML
gulp.task('html', function() {
    gulp.src('./source/*.html')
        .pipe(gulp.dest('./'));
});

// SCSS
gulp.task('scss', function() {
    gulp.src('./source/res/styles/*.scss')
        .pipe(scss().on('error', scss.logError))
        // .pipe(autoprefixer({
        //     browsers: ['last 5 versions'],
        //     cascade: false
        // }))
        .pipe(gulp.dest('./styles'));
});

// JavaScript
gulp.task('js', function() {
    let src = [
        './source/res/scripts/jquery-3.2.1.min.js',
        './source/res/scripts/Table.js',
        './source/res/scripts/Row.js',
        './source/res/scripts/Summary.js',
        './source/res/scripts/script.js'
    ];
    return gulp.src(src)
        .pipe(concat('script.js'))
        .pipe(gulp.dest('./scripts'));
});

gulp.task('watch', function() {
    gulp.watch('*.html').on('change', browserSync.reload);
    gulp.watch('styles/*.css').on('change', browserSync.reload);
    gulp.watch('scripts/*.js').on('change', browserSync.reload);
});

gulp.task('default', ['html', 'scss', 'js', 'browser-sync']);